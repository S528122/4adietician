//
//  CashInTakeTableViewCell.swift
//  Dietician
//
//  Created by sainathreddy on 4/20/17.
//  Copyright © 2017 Runyon,Jaque K. All rights reserved.
//

import UIKit

class CashInTakeTableViewCell: UITableViewCell {

    @IBOutlet weak var activity: UILabel!
    @IBOutlet weak var calories: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
