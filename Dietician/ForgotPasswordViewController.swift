//
//  ForgotPasswordViewController.swift
//  Dietician
//
// This is for forgot password functionality where an email will be sent to registered user for password reset
//  Created by Runyon,Jaque K on 3/5/17.
//  Copyright © 2017 Runyon,Jaque K. All rights reserved.
//

import UIKit
import Parse

class ForgotPasswordViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    @IBOutlet weak var emailTF: UITextField!
    

    
    @IBAction func sendMail(_ sender: UIButton) {
    
        let userMail = emailTF.text
        
        PFUser.requestPasswordResetForEmail(inBackground: userMail!) { (success, error: Error?) -> Void in
            
            if (success){
                
                let successMessage = "Email message was sent to you at \(userMail)"
                
                self.displayMessagge(theMessage: successMessage)
                
                return
                
            }
            
            if(error != nil){
                
                let errorMessage:String  = "No such email exists"
                
                self.displayMessagge(theMessage: errorMessage)
                
            }
            
            
            
        }
    
    
    }
    
    
    
    func displayMessagge(theMessage: String)
    
    {
    var myAlert = UIAlertController(title: "Alert!", message: theMessage, preferredStyle: UIAlertControllerStyle.alert)
    
    
        
    let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default) { (action) in
        
        
      
        self.dismiss(animated: true,completion: nil);
        
       
    }
    
    
    
    
    
    myAlert.addAction(okAction)
    
    
    
    
    
    self.present(myAlert, animated: true, completion: nil)
    
    
    
    
    
    
    
    }
    
    
    
    
    
    
    
    
    

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
