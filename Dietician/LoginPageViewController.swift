//
//  LoginPageViewController.swift
//  Dietician
//
//  Created by Ekka,Abhinav on 4/2/17.
//  Copyright © 2017 Runyon,Jaque K. All rights reserved.
//

import UIKit
import Parse
import Bolts


class LoginPageViewController: UIViewController {
    var username:String!
    var password:String!

    override func viewDidLoad() {
        super.viewDidLoad()
        textUsername.text = username
        textPassword.text = password

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        textUsername.text! = "karunagangidi585@gmail.com"
        textPassword.text! = "karuna_reddy"
    }
    @IBOutlet weak var textUsername: UITextField!

    @IBOutlet weak var textPassword: UITextField!

    
    @IBAction func unwindToHome(sender: UIStoryboardSegue)
    {
           }
   
     @IBAction func signUpBTN(_ sender: Any)
     {
        PFUser.logInWithUsername(inBackground: textUsername.text!, password: textPassword.text!, block:{(user, error) -> Void in
            if error != nil{
                print("error",error)
            }
            else {
                // Everything went alright here
              //  self.displayAlertWithTitle("Success!", message:"Login successful")
                self.performSegue(withIdentifier: "userHome", sender: nil)
            }
        })
        
        
     

        
        // Retrieving the info from the text fields
        
     }
    func displayAlertWithTitle(_ title:String, message:String){
        let alert:UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let defaultAction:UIAlertAction =  UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(defaultAction)
        self.present(alert, animated: true, completion: nil)
        
    }
    @IBAction func unwind(segue:UIStoryboard){
        
    }
    /*
     // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
