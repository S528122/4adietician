//
//  FirstViewController.swift
//  Dietician
//
//This is the home page that will be displayed immediately after user signs in
//
//Created by Runyon,Jaque K on 3/5/17.
//  Copyright © 2017 Runyon,Jaque K. All rights reserved.
//

import UIKit
import Parse
import Charts


class FirstViewController: UIViewController {
    var workout:Double = 0.0
    var calIntake:Double = 0.0
    
    var a:[String] = []
    var b:[String] = []
    
    var x:Double = 0.0
    var y:Double = 0.0
    
    var totIN:Double = 0.0
    var totBT:Double = 0.0
    
    //This is an outlet for displaying the intake calories
    @IBOutlet weak var intakeLBL: UILabel!
    
    //This is an outlet for the view to display barchart
    @IBOutlet weak var subContainerView: BarChartView!
    @IBOutlet weak var workoutLBL: UILabel!
    

    override func viewDidLoad() {
        super.viewDidLoad()
           // intakeLBL.text = calIntake
        
           //  workoutLBL.text! = workout

     //  local2 = (UIApplication.shared.delegate as! AppDelegate).global
        // Do any additional setup after loading the view, typically from a nib.
        
        
        
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        
        intakeLBL.text! = String(totIN)
        
        
        
        workoutLBL.text! = String(totBT)
        
        
        
        
        //query to retrieve results
        let query1 = PFQuery(className: "CaloriesIntake")
        
        
        
        do {
            
            let results: [PFObject] = try query1.findObjects()
            
            print(results)
            
            
            
            
            
            for object in results
                
            {
                
                if(!a.contains(object["calIntake"] as! String) )
                    
                {
                    
                    
                    
                    
                    
                    self.a.append(object["calIntake"] as! String)
                    
                    totIN += Double(object["calIntake"] as! String)!
                    
                    
                    
                    print(" cal intake is \(object["calIntake"] as! String)")
                    
                    
                    
                }
                
            }
            
            
            
        }
            
            
            
        catch
            
        {
            
            print(error)
            
        }
        
        
        
        let query2 = PFQuery(className: "CaloriesBurnt")
        
        
        
        do {
            
            let result: [PFObject] = try query2.findObjects()
            
            print(result)
            
            for object in result
                
            {
                
                
                
                if(!b.contains(object["calBurnt"] as! String) )
                    
                {
                    
                    
                    
                    self.b.append(object["calBurnt"] as! String)
                    
                    
                    
                    totBT += Double(object["calBurnt"] as! String)!
                    
                    
                   
                    print(" cal Burnt is \(object["calBurnt"] as! String)")
                    
                }
                
                
                
            }
            
        }
            
        catch
            
        {
            
            print(error)
            
        }
        
        
        
        print("Cal Intake array is  \(a) ")
        
        print("Cal Burnt array is  \(b) ")
        
        
        
        
        
        
        
        
        
        intakeLBL.text! = String(totIN)
        
        
        
        workoutLBL.text! = String(totBT)
        
        
        
        
        
        self.view.reloadInputViews()
    
        
//        for i in a
//        {
//            totIN += Double(i)!
//        }
//        
//        
//        
//        for l in b
//        {
//            totBT += Double(l)!
//        }

        
    
    
    
        intakeLBL.text! = String(totIN)
        
        workoutLBL.text! = String(totBT)
        
        
        //Bar Chart
        // 1. create chart view
        //let chart = BarChartView(frame: self.subContainerView.frame)
        
        // 2. generate chart data entries
        let yVals: [Double] = [ totIN, totBT]
        var entries = [ BarChartDataEntry]()
        for (i, v) in yVals.enumerated() {
            let entry = BarChartDataEntry()
            entry.x = Double( i)
            entry.y = v
            print("\(i) \(v)")
            
            entries.append( entry)
        }
        
        // 3. chart setup
        let set = BarChartDataSet( values: entries, label: "Calorie Levels")
        let data = BarChartData( dataSet: set)
        subContainerView.data = data
        // no data text
        subContainerView.noDataText = "No data available"
        // user interaction
        subContainerView.isUserInteractionEnabled = false
        
        // 3a. style
//        subContainerView.backgroundColor = UIColor.blue
        //chart.backgroundColor = Palette.Background
        subContainerView.drawValueAboveBarEnabled = false
        subContainerView.xAxis.drawGridLinesEnabled = false
        subContainerView.leftAxis.drawGridLinesEnabled = false
        subContainerView.rightAxis.drawGridLinesEnabled = false
        
        subContainerView.leftAxis.drawLabelsEnabled = true
        subContainerView.rightAxis.drawLabelsEnabled = false
        //subContainerView.leftAxis.labelTextColor = Palette.InfoText
        
        // 3b. animation
        subContainerView.animate(xAxisDuration:  1.0)
        
        //subContainerView.legend.textColor = Palette.InfoText
        subContainerView.chartDescription = nil
        
        
        // 4. add chart to UI
        //self.subContainerView.addSubview( chart)
        
    }

  
    override func viewDidAppear(_ animated: Bool) {
        //  intakeLBL.text! = calIntake
        
       //   workoutLBL.text! = workout

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
   
    //Logout action on logout button
    @IBAction func logoutAction(_ sender: UIBarButtonItem) {
        
        PFUser.logOut()
        
        let login = storyboard?.instantiateInitialViewController()
        self.present(login!, animated: true, completion: nil)
        

        
    }
    
    
    

    @IBAction func unwindToHome(sender: UIStoryboardSegue)
    {
    }

    
    

 }

