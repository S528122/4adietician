//
//  AddWorkoutViewController.swift
//  Dietician
//
//This viewcontroller is to give data for number of calories burnt
//  Created by Ekka,Abhinav on 4/11/17.
//  Copyright © 2017 Runyon,Jaque K. All rights reserved.
//

import UIKit
import Parse
class AddWorkoutViewController: UIViewController, UITextFieldDelegate {
//var local1 = Calculation()
    override func viewDidLoad() {
        super.viewDidLoad()
        calBurntTF.keyboardType = UIKeyboardType.numberPad
        calBurntTF.delegate = self
    // local1 = (UIApplication.shared.delegate as! AppDelegate).global
        // Do any additional setup after loading the view.
             
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK: TextField Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        calBurntTF.resignFirstResponder()
        return true
    }
    
    @IBOutlet weak var activityTF: UITextField! // an outlet for activity text field
    
    @IBOutlet weak var calBurntTF: UITextField! // an outlet for activity text field
    
    
    // add workout button
    @IBAction func addWorkout(_ sender: UIButton) {
        let inTake = PFObject(className: "CaloriesBurnt")
        
        inTake["workoutActivity"] = self.activityTF.text!
        inTake["calBurnt"] = self.calBurntTF.text!
        
        inTake.saveInBackground { (successful, error) in
            if successful
            {
                
            }
            else
            {
                
            }
        }

    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        // Get the new view controller using segue.destinationViewController.
//        // Pass the selected object to the new view controller.
//        let home = segue.destination as! FirstViewController
//        
//        print(calBurntTF.text!)
//        home.workout += Double(calBurntTF.text!)!
     
        
        }
    
    @IBAction func backBTN(_ sender: UIBarButtonItem) {
    }
    
    
    }



