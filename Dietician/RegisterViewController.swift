//
//  RegisterViewController.swift
//  Dietician
//
//This viewcontroller is for user registration
//  Created by Ekka,Abhinav on 4/2/17.
//  Copyright © 2017 Runyon,Jaque K. All rights reserved.
//

import UIKit
import Parse

class RegisterViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var emailTF: UITextField!   //outlet for email textfield

    @IBOutlet weak var usernameTF: UITextField!  //outlet for email textfield
    
    @IBOutlet weak var passwordTF: UITextField!   //outlet for email textfield
    @IBAction func registerBTN(_ sender: UIButton)  //actiion triggered when user taps register button
    {
        // Defining the user object
        let user = PFUser()
        user.username = emailTF.text! //usernameTF.text!
        user.password = passwordTF.text!
        user.email = emailTF.text!
        
        // Signing up using the Parse API
        user.signUpInBackground( block: {
            (success, error) -> Void in
            if let error = error as NSError? {
                let errorString = error.userInfo["error"] as? NSString
                // In case something went wrong, use errorString to get the error
                self.displayAlertWithTitle("Something has gone wrong", message:"\(errorString)")
            }
            else {
                // Everything went okay
                self.displayAlertWithTitle("Success!", message:"Registration was successful")
                
                
                let emailVerified = user["emailVerified"]
                if emailVerified != nil && (emailVerified as! Bool) == true {
                    // Everything is fine
                }
                else {
                    // The email has not been verified, so logout the user
                    PFUser.logOut()
                }
            }
        })
    }
    
    
    //function to display alert message about his registration whether successfull or not
    func displayAlertWithTitle(_ title:String, message:String){
        let alert:UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let defaultAction:UIAlertAction =  UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(defaultAction)
        self.present(alert, animated: true, completion: nil)
        
    }
    
     // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    //
//  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
////        // Get the new view controller using segue.destinationViewController.
////        // Pass the selected object to the new view controller.
//     let destination = segue.destination as! LoginPageViewController
//       destination.username = usernameTF.text!
//    destination.password = passwordTF.text!
////        
//    }
    
    @IBAction func cancelACT(_ sender: UIButton) {
        //cancel button that goes to login page
        PFUser.logOut()
        
        let login = storyboard?.instantiateInitialViewController()
        self.present(login!, animated: true, completion: nil)
        

        
        
    }
}
