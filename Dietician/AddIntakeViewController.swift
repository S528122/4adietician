//
//  AddIntakeViewController.swift
//  Dietician
//
//This ViewController allows user to give calories intake and the type of food.
//  Created by Ekka,Abhinav on 4/11/17.
//  Copyright © 2017 Runyon,Jaque K. All rights reserved.
//

import UIKit
import Parse

class AddIntakeViewController: UIViewController, UITextFieldDelegate {
  //  var local = Calculation()
   //an outlet for intake activity textfield
    @IBOutlet weak var intakeTF: UITextField!
    //an outlet for number of calories user enters in text field
    @IBOutlet weak var calIntakeTF: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        calIntakeTF.keyboardType = UIKeyboardType.numberPad
        calIntakeTF.delegate = self
       // local = (UIApplication.shared.delegate as! AppDelegate).global
        // Do any additional setup after loading the view.
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        

        // Dispose of any resources that can be recreated.
    }
    // MARK: TextField Delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        calIntakeTF.resignFirstResponder()
        return true
    }
  
    // an action for addcalories button.
    @IBAction func totalCaloriesBTN(_ sender: Any) {
//        let destination = segue.destiantion as!
//        let cal = Double(calIntakeTF.text!)!
//        local.calculate(calories: cal)
        let inTake = PFObject(className: "CaloriesIntake")
        
        inTake["inTakeActivity"] = self.intakeTF.text!
        print(inTake["inTakeActivity"])
        inTake["calIntake"] = self.calIntakeTF.text!
        
        inTake.saveInBackground { (successful, error) in
            if successful
            {
                
            }
            else
            {
                
            }
        }
        self.resignFirstResponder()
    }
   
//  
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

//        let home = segue.destination as! FirstViewController
//    
//    print(calIntakeTF.text!)
//        home.calIntake += Double(calIntakeTF.text!)!
    }
    @IBAction func backButton(_ sender: UIBarButtonItem) {
    }
    
    
    
    
}
