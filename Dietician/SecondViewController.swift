//
//  SecondViewController.swift
//  Dietician
//
// We dint implement this class
//  Created by Runyon,Jaque K on 3/5/17.
//  Copyright © 2017 Runyon,Jaque K. All rights reserved.
//

import UIKit
import Charts

class SecondViewController: UIViewController {

    
    @IBOutlet weak var weeklyBarChart: BarChartView!
    
    @IBOutlet weak var monthlyBarChart: BarChartView!
    
    var weeklyData:[Double] = [1,2,3]
    var monthlyData:[Double] = [3,2]
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
    }

    override func viewWillAppear(_ animated: Bool) {
        setUpBarChart(yVals: weeklyData, subContainerView: weeklyBarChart)
        setUpBarChart(yVals: monthlyData, subContainerView: monthlyBarChart)
    }
    func setUpBarChart(yVals:[Double], subContainerView:BarChartView){
        //Bar Chart
        // 1. create chart view
        //let chart = BarChartView(frame: self.subContainerView.frame)
        
        // 2. generate chart data entries
        var entries = [ BarChartDataEntry]()
        for (i, v) in yVals.enumerated() {
            let entry = BarChartDataEntry()
            entry.x = Double( i)
            entry.y = v
            print("\(i) \(v)")
            
            entries.append( entry)
        }
        
        // 3. chart setup
        let set = BarChartDataSet( values: entries, label: "Calorie Levels")
        let data = BarChartData( dataSet: set)
        subContainerView.data = data
        // no data text
        subContainerView.noDataText = "No data available"
        // user interaction
        subContainerView.isUserInteractionEnabled = false
        
        // 3a. style
        //        subContainerView.backgroundColor = UIColor.blue
        //chart.backgroundColor = Palette.Background
        subContainerView.drawValueAboveBarEnabled = false
        subContainerView.xAxis.drawGridLinesEnabled = false
        subContainerView.leftAxis.drawGridLinesEnabled = false
        subContainerView.rightAxis.drawGridLinesEnabled = false
        
        subContainerView.leftAxis.drawLabelsEnabled = true
        subContainerView.rightAxis.drawLabelsEnabled = false
        //subContainerView.leftAxis.labelTextColor = Palette.InfoText
        
        // 3b. animation
        subContainerView.animate(xAxisDuration:  1.0)
        
        //subContainerView.legend.textColor = Palette.InfoText
        subContainerView.chartDescription = nil
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

