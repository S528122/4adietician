//
//  ThirdViewController.swift
//  Dietician
//This class displays data based on date and displays in a table. We even implemented date picker
//  Created by Runyon,Jaque K on 3/5/17.
//  Copyright © 2017 Runyon,Jaque K. All rights reserved.
//

import UIKit
import Charts
import Parse
class ThirdViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

//An outlet for for table view to display data
    @IBOutlet weak var tview: UITableView!
    var CaloriesInTake:[PFObject]!
    var CaloriesOutTake:[PFObject]!
    let datePicker = UIDatePicker()
    override func viewDidLoad() {
        super.viewDidLoad()
createDatePicker()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //An outlet for date textfield
    @IBOutlet weak var dateTF: UITextField!

    func createDatePicker() {

        datePicker.datePickerMode = .date
        // toolbar
        
        let toolbar = UIToolbar()
        
        toolbar.sizeToFit()
  
        //bar button item
        
        let donebutton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressed))
        
        toolbar.setItems([donebutton],  animated: false)
        
        dateTF.inputAccessoryView = toolbar
        
        //assigning date picker to text field
        
        dateTF.inputView = datePicker
        
    }
    
    
    
    //func when  user presses done in date picker
    func donePressed()
    {
        let dateFormatter = DateFormatter()
        //dateFormatter.dateFormat = "MM/dd/yyyy"
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .none
        
        dateTF.text = dateFormatter.string(from: datePicker.date)
        
        self.view.endEditing(true)
    }
    
    
   // @IBOutlet weak var resultView: UITextView!
   //number of sections in table
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    //number of rows in each section
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            if CaloriesInTake != nil {
            return CaloriesInTake.count
            }
            else {
                return 0
            }
        case 1:
            if CaloriesOutTake != nil {
            return CaloriesOutTake.count
            }
            else{
                return 0
            }
        default:
            return 0
        }
    }
    //to populate data at specific cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tview.dequeueReusableCell(withIdentifier: "cell") as! CashInTakeTableViewCell
        
        if indexPath.section == 0 {
            cell.activity.text = (CaloriesInTake[indexPath.row])["inTakeActivity"] as! String
            cell.calories.text = (CaloriesInTake[indexPath.row])["calIntake"] as! String
        }
        else if indexPath.section == 1 {
            cell.activity.text = (CaloriesOutTake[indexPath.row])["workoutActivity"] as! String
            cell.calories.text = (CaloriesOutTake[indexPath.row])["calBurnt"] as! String
        }
        return cell
    }
    //title for each section
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "In Take Activities"
        case 1:
            return "Out Take Activities"
        default:
            return "Activities"
        }
    }
//action method for get data button
    @IBAction func getData(_ sender: UIButton) {
        
        self.tview.isHidden = false
        
        var df = DateFormatter()
        
        df.dateFormat = "MM/dd/yy, HH:mm"
        //df.dateStyle = .short
        //df.timeStyle = .none
        let date1 = df.date(from: dateTF.text!+", 00:00")
        
        print(date1)
        let query1 = PFQuery(className: "CaloriesIntake")
        
        
        
      //query1.whereKey("updatedAt" , greater : date1!)
       
     query1.whereKey("updatedAt",  lessThan: date1)

        query1.findObjectsInBackground { (objects, error) -> Void in
            
            print("nowors", objects?.count)
            print("JFKjkf")
            
            
            if(error == nil)
            {
                self.CaloriesInTake = objects
                self.tview.reloadData()
                //self.resultView.text = ""
                for ob in objects!{
                    
                  //  print(ob["updatedAt"] as! Date )
                    
//                    if (ob["updateAt"] as! String).contains(self.dateTF.text!)
//                    {
                    print("karuna")
                    
                    print(ob["calIntake"] as! String)
                    //self.resultView.text.append("\(ob["inTakeActivity"]!) - \(ob["calIntake"]! ) calories  \n")
                
                }
            }
    
        }
        
        
        let query2 = PFQuery(className: "CaloriesBurnt")
        query2.whereKey("updatedAt", lessThanOrEqualTo: date1)
        query2.findObjectsInBackground { (objects, error) in
            if error == nil {
                self.CaloriesOutTake = objects
                self.tview.reloadData()
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */


}
